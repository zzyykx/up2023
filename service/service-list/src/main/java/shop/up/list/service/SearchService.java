package shop.up.list.service;

import shop.up.model.list.SearchParam;
import shop.up.model.list.SearchResponseVo;

import java.io.IOException;

/**
 * @Author: Tizzy
 * @Date: 2023/4/7 15:18
 */
public interface SearchService {

    /**
     * 搜索列表
     * @param searchParam
     * @return
     * @throws IOException
     */
    SearchResponseVo search(SearchParam searchParam) throws IOException;

    /**
     * 更新热点
     * @param skuId
     */
    void incrHotScore(Long skuId);

    /**
     * 上架商品列表
     * @param skuId
     */
    void upperGoods(Long skuId);

    /**
     * 下架商品列表
     * @param skuId
     */
    void lowerGoods(Long skuId);
}
