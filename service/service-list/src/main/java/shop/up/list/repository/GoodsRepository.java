package shop.up.list.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import shop.up.model.list.Goods;

/**
 * @Author: Tizzy
 * @Date: 2023/4/7 15:20
 */
public interface GoodsRepository extends ElasticsearchRepository<Goods,Long> {
}
