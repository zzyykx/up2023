package shop.up.cart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import shop.up.model.cart.CartInfo;

/**
 * @Author: Tizzy
 * @Date: 2023/4/12 21:17
 */
public interface CartInfoMapper extends BaseMapper<CartInfo> {
}
