package shop.up.cart.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import shop.up.cart.mapper.CartInfoMapper;
import shop.up.cart.service.CartAsyncService;
import shop.up.cart.service.CartService;
import shop.up.common.constant.RedisConst;
import shop.up.common.util.DateUtil;
import shop.up.model.cart.CartInfo;
import shop.up.model.product.SkuInfo;
import shop.up.product.client.ProductFeignClient;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Author: Tizzy
 * @Date: 2023/4/12 21:19
 */
@Service
public class CartServiceImpl implements CartService {

    @Resource
    private CartInfoMapper cartInfoMapper;

    @Resource
    private ProductFeignClient productFeignClient;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private CartAsyncService cartAsyncService;

    @Override
    public List<CartInfo> getCartCheckedList(String userId) {
        List<CartInfo> cartInfoList = new ArrayList<>();
        String cartKey = getCartKey(userId);
        List<CartInfo> cartCacheInfoList = redisTemplate.opsForHash().values(cartKey);
        if (cartCacheInfoList != null && cartCacheInfoList.size() > 0){
            for (CartInfo info : cartCacheInfoList) {
                if (info.getIsChecked().intValue() == 1) {
                    cartInfoList.add(info);
                }
            }
        }
        return cartInfoList;
    }

    @Override
    public void addToCart(Long skuId, String userId, Integer skuNum) {
        /*
            1.  添加商品之前，先看一下购物车中是否有该商品
                true:
                    商品数量相加
                false:
                    直接加入购物车
            2.    将数据同步到redis！
         */
        //  数据类型hash + key hset(key,field,value)
        //  key = user:userId:cart ,谁的购物车 field = skuId value = cartInfo
        String cartKey = getCartKey(userId);
        //判断缓存中是否有购物车缓存
        if (!redisTemplate.hasKey(cartKey)) {
            //没有缓存，传数据库并更新缓存
            loadCartCache(userId);
        }
        CartInfo cartInfoExist = (CartInfo) redisTemplate.boundHashOps(cartKey).get(skuId.toString());
        //判断购物车中是否已存在该商品
        if (cartInfoExist != null) {
            // 数量相加
            cartInfoExist.setSkuNum(cartInfoExist.getSkuNum() + skuNum);
            // 获取最新价格
            cartInfoExist.setSkuPrice(productFeignClient.getSkuPrice(skuId));
            //  修改更新时间
            cartInfoExist.setUpdateTime(new Timestamp(new Date().getTime()));
            //  再次添加商品时，默认选中状态。
            cartInfoExist.setIsChecked(1);
            //  修改数据库执行语句
            // 异步进行更新数据库
            cartAsyncService.updateCartInfo(cartInfoExist);
        }else {
            //  第一次添加购物车
            CartInfo cartInfo = new CartInfo();
            SkuInfo skuInfo = productFeignClient.getSkuInfo(skuId);
            cartInfo.setUserId(userId);
            cartInfo.setSkuId(skuId);
            //  在初始化的时候，添加购物车的价格 = skuInfo.price
            cartInfo.setCartPrice(skuInfo.getPrice());
            //  数据库不存在的，购物车的价格 = skuInfo.price
            cartInfo.setSkuPrice(skuInfo.getPrice());
            cartInfo.setSkuNum(skuNum);
            cartInfo.setImgUrl(skuInfo.getSkuDefaultImg());
            cartInfo.setSkuName(skuInfo.getSkuName());
            cartInfo.setCreateTime(new Timestamp(new Date().getTime()));
            cartInfo.setUpdateTime(new Timestamp(new Date().getTime()));
            //  异步保存到数据库
            cartAsyncService.saveCartInfo(cartInfo);
            cartInfoExist = cartInfo;
        }
        //  将 cartInfoExist 最新数据更新到缓存
        redisTemplate.boundHashOps(cartKey).put(skuId.toString(),cartInfoExist);
        //  设置过期时间
        setCartKeyExpire(cartKey);
    }

    /**
     * 通过用户Id 查询购物车列表
     * @param userId
     * @param userTempId
     * @return
     */
    @Override
    public List<CartInfo> getCartList(String userId, String userTempId) {
        // 返回的集合对象
        List<CartInfo> cartInfoList = new ArrayList<>();
        // 未登录：临时用户Id 获取未登录的购物车数据
        if (StringUtils.isEmpty(userId)) {
            cartInfoList = getCartList(userTempId);
            return cartInfoList;
        }

        /*
         1. 准备合并购物车
         2. 获取未登录的购物车数据
         3. 如果未登录购物车中有数据，则进行合并 合并的条件：skuId 相同 则数量相加，合并完成之后，删除未登录的数据！
         4. 如果未登录购物车没有数据，则直接显示已登录的数据
          */
        //已登录
        if (!StringUtils.isEmpty(userId)) {
            List<CartInfo> cartInfoArrayList = this.getCartList(userTempId);
            cartInfoList = getCartList(userId);
            if (!CollectionUtils.isEmpty(cartInfoArrayList)) {
                // 如果未登录购物车中有数据，则进行合并 合并的条件：skuId 相同
                cartInfoList = mergeToCartList(cartInfoArrayList, userId);
                // 删除未登录购物车数据
                deleteCartList(userTempId);
            }
            if (StringUtils.isEmpty(userTempId) || CollectionUtils.isEmpty(cartInfoArrayList)) {
                // 根据什么查询？userId
                cartInfoList = getCartList(userId);
            }
            return cartInfoList;
        }
        return cartInfoList;

    }

    @Override
    public void checkCart(String userId, Integer isChecked, Long skuId) {
        cartAsyncService.checkCart(userId, isChecked, skuId);

        String cartKey = getCartKey(userId);
        BoundHashOperations<String, String, CartInfo> hashOperations = redisTemplate.boundHashOps(cartKey);
        // 先获取用户选择的商品
        if (hashOperations.hasKey(skuId.toString())) {
            CartInfo cartInfoUpd = hashOperations.get(skuId.toString());
            // cartInfoUpd 写会缓存
            cartInfoUpd.setIsChecked(isChecked);
            // 更新缓存
            hashOperations.put(skuId.toString(), cartInfoUpd);
            // 设置过期时间
            setCartKeyExpire(cartKey);
        }
    }

    @Override
    public void deleteCart(Long skuId, String userId) {
        String cartKey = getCartKey(userId);
        cartAsyncService.deleteCartInfo(userId, skuId);

        //获取缓存对象
        BoundHashOperations<String, String, CartInfo> hashOperations = redisTemplate.boundHashOps(cartKey);
        if (hashOperations.hasKey(skuId.toString())){
            hashOperations.delete(skuId.toString());
        }
    }

    private void deleteCartList(String userTempId) {
        // 删除数据库，删除缓存
        // delete from userInfo where userId = ?userTempId
        cartAsyncService.deleteCartInfo(userTempId);

        String cartKey = getCartKey(userTempId);
        Boolean flag = redisTemplate.hasKey(cartKey);
        if (flag){
            redisTemplate.delete(cartKey);
        }
    }

    private List<CartInfo> mergeToCartList(List<CartInfo> cartInfoNoLoginList, String userId) {
        //  根据用户 Id 查询登录的购物车数据
        List<CartInfo> cartInfoLoginList = getCartList(userId);
        Map<Long, CartInfo> longCartInfoMap = cartInfoLoginList.stream().
                collect(Collectors.toMap(CartInfo::getSkuId, cartInfo -> cartInfo));
        //  判断未登录的skuId 在已登录的Map 中是否有这个key
        //  遍历集合
        for (CartInfo cartInfo : cartInfoNoLoginList) {
            Long skuId = cartInfo.getSkuId();
            if (longCartInfoMap.containsKey(skuId)) {
                CartInfo info = longCartInfoMap.get(skuId);
                info.setSkuNum(info.getSkuNum() + cartInfo.getSkuNum());
                info.setUpdateTime(new Timestamp(new Date().getTime()));
                //  合并购物车时的选择状态！
                //  以未登录选中状态为基准
                if (cartInfo.getIsChecked().intValue()==1){
                    info.setIsChecked(1);
                }
                // 因为info可能是缓存中获取的，没有id所以不能直接update
                QueryWrapper<CartInfo> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("user_id",info.getUserId());
                queryWrapper.eq("sku_id",info.getSkuId());
                //  cartInfo第一个参数相当于 ，第二个参数相当于更新条件
                cartInfoMapper.update(info,queryWrapper);
            }else {
                //  从临时用户购物车来，需要改一下 id
                cartInfo.setUserId(userId);
                //  添加时间
                cartInfo.setCreateTime(new Timestamp(new Date().getTime()));
                cartInfo.setUpdateTime(new Timestamp(new Date().getTime()));
                cartInfoMapper.insert(cartInfo);
            }
        }
        //  从数据库中获取到最新合并的数据，然后放入缓存
        List<CartInfo> cartInfoList = this.loadCartCache(userId);
        return cartInfoList;
    }

    /**
     * 根据用户获取购物车
     * @param userId
     * @return
     */
    private List<CartInfo> getCartList(String userId) {
        // 声明一个返回的集合对象
        List<CartInfo> cartInfoList = new ArrayList<>();
        if (StringUtils.isEmpty(userId))
            return cartInfoList;

    /*
    1.  根据用户Id 查询 {先查询缓存，缓存没有，再查询数据库}
     */
        // 定义key user:userId:cart
        String cartKey = this.getCartKey(userId);
        // 获取数据
        cartInfoList = redisTemplate.opsForHash().values(cartKey);
        if (!CollectionUtils.isEmpty(cartInfoList)) {
            // 购物车列表显示有顺序：按照商品的更新时间 降序
            cartInfoList.sort(new Comparator<CartInfo>() {
                @Override
                public int compare(CartInfo o1, CartInfo o2) {
                    return DateUtil.truncatedCompareTo(o2.getUpdateTime(),o1.getUpdateTime(),Calendar.SECOND);
                }
            });
            return cartInfoList;
        } else {
            // 缓存中没用数据！
            cartInfoList = loadCartCache(userId);
            return cartInfoList;
        }
    }


    /**
     * 通过userId 查询购物车并放入缓存！
     * @param userId
     * @return
     */
    public List<CartInfo> loadCartCache(String userId) {
        QueryWrapper queryWrapper = new QueryWrapper<CartInfo>();
        queryWrapper.eq("user_id", userId);
        queryWrapper.orderByDesc("update_time");
        List<CartInfo> cartInfoList = cartInfoMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(cartInfoList)) {
            return cartInfoList;
        }
        // 将数据库中的数据查询并放入缓存
        HashMap<String, CartInfo> map = new HashMap<>();
        for (CartInfo cartInfo : cartInfoList) {
            BigDecimal skuPrice = productFeignClient.getSkuPrice(cartInfo.getSkuId());
            cartInfo.setSkuPrice(skuPrice);
            map.put(cartInfo.getSkuId().toString(), cartInfo);
        }

        // 定义key user:userIed:cart
        String cartKey = this.getCartKey(userId);
        redisTemplate.opsForHash().putAll(cartKey, map);
        // 设置过期时间
        setCartKeyExpire(cartKey);
        return cartInfoList;
    }



    /**
     * 获取购物车的key
     * @param userId
     * @return
     */
    private String getCartKey(String userId) {
        //定义key user:userId:cart
        return RedisConst.USER_KEY_PREFIX + userId + RedisConst.USER_CART_KEY_SUFFIX;
    }

    /**
     * 设置过期时间
     * @param cartKey
     */
    private void setCartKeyExpire(String cartKey) {
        redisTemplate.expire(cartKey, RedisConst.USER_CART_EXPIRE, TimeUnit.SECONDS);
    }
}
