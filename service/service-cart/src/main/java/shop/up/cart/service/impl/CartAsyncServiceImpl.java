package shop.up.cart.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import shop.up.cart.mapper.CartInfoMapper;
import shop.up.cart.service.CartAsyncService;
import shop.up.model.cart.CartInfo;

import javax.annotation.Resource;

/**
 * @Author: Tizzy
 * @Date: 2023/4/12 21:47
 */
@Service
public class CartAsyncServiceImpl implements CartAsyncService {

    @Resource
    private CartInfoMapper cartInfoMapper;

    @Override
    @Async
    public void updateCartInfo(CartInfo cartInfo) {
        QueryWrapper<CartInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",cartInfo.getUserId());
        queryWrapper.eq("sku_id",cartInfo.getSkuId());
        //  更新数据
        cartInfoMapper.update(cartInfo,queryWrapper);
    }

    @Override
    @Async
    public void saveCartInfo(CartInfo cartInfo) {
        cartInfoMapper.insert(cartInfo);
    }

    @Async
    @Override
    public void deleteCartInfo(String userId) {
        cartInfoMapper.delete(new QueryWrapper<CartInfo>().eq("user_id", userId));
    }

    @Override
    public void checkCart(String userId, Integer isChecked, Long skuId) {
        CartInfo cartInfo = new CartInfo();
        cartInfo.setIsChecked(isChecked);
        QueryWrapper queryWrapper = new QueryWrapper<CartInfo>();
        queryWrapper.eq("user_id", userId);
        queryWrapper.eq("sku_id", skuId);
        cartInfoMapper.update(cartInfo, queryWrapper);
    }

    @Async
    @Override
    public void deleteCartInfo(String userId, Long skuId) {
        cartInfoMapper.delete(new QueryWrapper<CartInfo>().eq("user_id", userId).eq("sku_id", skuId));
    }
}
