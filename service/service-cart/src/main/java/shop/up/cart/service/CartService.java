package shop.up.cart.service;

import shop.up.model.cart.CartInfo;

import java.util.List;

/**
 * @Author: Tizzy
 * @Date: 2023/4/12 21:18
 */
public interface CartService {

     /* 根据用户Id查询购物车最新数据并放入缓存
     * @param userId
     * @return
     */
    List<CartInfo> loadCartCache(String userId);
    /**
     * 根据用户Id 查询购物车列表
     *
     * @param userId
     * @return
     */
    List<CartInfo> getCartCheckedList(String userId);

    /**
     * 添加购物车 用户Id，商品Id，商品数量。
     * @param skuId
     * @param userId
     * @param skuNum
     */
    void addToCart(Long skuId, String userId, Integer skuNum);

    /**
     * 通过用户Id 查询购物车列表
     * @param userId
     * @param userTempId
     * @return
     */
    List<CartInfo> getCartList(String userId, String userTempId);

    /**
     * 更新选中状态
     *
     * @param userId
     * @param isChecked
     * @param skuId
     */
    void checkCart(String userId, Integer isChecked, Long skuId);

    /**
     * 删除
     * @param userId
     * @param skuId
     */
    void deleteCart(Long skuId, String userId);

}
