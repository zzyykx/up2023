package shop.up.order.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import shop.up.model.order.OrderDetail;


public interface OrderDetailMapper extends BaseMapper<OrderDetail> {
}