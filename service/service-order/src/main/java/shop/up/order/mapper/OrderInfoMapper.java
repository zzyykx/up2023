package shop.up.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import shop.up.model.order.OrderInfo;

/**
 * @Author: Tizzy
 * @Date: 2023/4/13 21:01
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {
}
