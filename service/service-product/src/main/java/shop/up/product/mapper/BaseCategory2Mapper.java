package shop.up.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import shop.up.model.product.BaseCategory2;

/**
 * @Author: Tizzy
 * @Date: 2023/3/28 10:18
 *
 * 二级分类Mapper接口
 */
@Mapper
public interface BaseCategory2Mapper extends BaseMapper<BaseCategory2> {
}
