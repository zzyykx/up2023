package shop.up.product.service.imp;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import shop.up.model.product.BaseTrademark;
import shop.up.product.mapper.BaseTrademarkMapper;
import shop.up.product.service.BaseTrademarkService;

/**
 * @Author: Tizzy
 * @Date: 2023/3/29 14:40
 */
@Service
public class BaseTrademarkServiceImpl extends ServiceImpl<BaseTrademarkMapper, BaseTrademark> implements BaseTrademarkService {

}
