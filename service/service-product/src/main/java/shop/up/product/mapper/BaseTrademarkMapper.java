package shop.up.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import shop.up.model.product.BaseTrademark;

/**
 * @Author: Tizzy
 * @Date: 2023/3/29 14:36
 */
public interface BaseTrademarkMapper extends BaseMapper<BaseTrademark> {
}
