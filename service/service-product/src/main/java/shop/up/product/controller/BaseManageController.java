package shop.up.product.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import shop.up.common.result.Result;
import shop.up.model.product.*;
import shop.up.product.service.imp.ManageServiceImp;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: Tizzy
 * @Date: 2023/3/28 13:11
 */

@Api(tags = "商品基础属性接口")
@RequestMapping("admin/product")
@RestController
public class BaseManageController {

    @Resource
    private ManageServiceImp manageServiceImp;


    /**
     * 根据属性id获取属性值
     * @param attrId
     * @return
     */

    @ApiOperation("根据属性id获取属性值")
    @GetMapping("getAttrValueList/{attrId}")
    public Result<List<BaseAttrValue>> getAttrValueList(@PathVariable("attrId") Long attrId) {
        BaseAttrInfo baseAttrInfo = manageServiceImp.getAttrInfo(attrId);
        List<BaseAttrValue> baseAttrValueList = baseAttrInfo.getAttrValueList();
        return Result.ok(baseAttrValueList);
    }
    /**
     * 保存平台属性方法
     * @param baseAttrInfo
     * @return
     */

    @ApiOperation("修改或添加平台属性")
    @PostMapping("saveAttrInfo")
    public Result saveAttrInfo(@RequestBody BaseAttrInfo baseAttrInfo) {
        // 前台数据都被封装到该对象中baseAttrInfo
        manageServiceImp.saveAttrInfo(baseAttrInfo);
        return Result.ok();
    }


    @ApiOperation("获取一级分类")
    @GetMapping("/getCategory1")
    public Result<List<BaseCategory1>> getCategory1(){
        return Result.ok(manageServiceImp.getCategory1());
    }

    @ApiOperation("获取二级分类")
    @GetMapping("/getCategory2/{category1Id}")
    public Result<List<BaseCategory2>> getCategory2(@PathVariable("category1Id")Long category1Id){
        return Result.ok(manageServiceImp.getCategory2(category1Id));
    }

    @ApiOperation("获取三级分类")
    @GetMapping("/getCategory3/{category2Id}")
    public Result<List<BaseCategory3>> getCategory3(@PathVariable("category2Id")Long category2Id){
        return Result.ok(manageServiceImp.getCategory3(category2Id));
    }

    @ApiOperation("根据分类id 获取获取平台属性")
    @GetMapping("attrInfoList/{category1Id}/{category2Id}/{category3Id}")
    public Result<List<BaseAttrInfo>> attrInfoList(@PathVariable("category1Id") Long category1Id,
                                                   @PathVariable("category2Id") Long category2Id,
                                                   @PathVariable("category3Id") Long category3Id) {
        List<BaseAttrInfo> baseAttrInfoList = manageServiceImp.getAttrInfoList(category1Id, category2Id, category3Id);
        return Result.ok(baseAttrInfoList);
    }
}
