package shop.up.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import shop.up.model.product.BaseCategoryView;

@Mapper
public interface BaseCategoryViewMapper extends BaseMapper<BaseCategoryView> {

}