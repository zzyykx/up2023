package shop.up.product.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.up.common.result.Result;
import shop.up.product.service.TestService;

import javax.annotation.Resource;

/**
 * @Author: Tizzy
 * @Date: 2023/4/4 19:44
 */
@RestController
@RequestMapping("admin/product/test")
public class TestController {

    @Resource
    private TestService testService;

    @GetMapping("testLock")
    public Result testLock() {
        testService.testLock();
        return Result.ok();
    }

    @GetMapping("read")
    public Result<String> read(){
        String msg = testService.readLock();
        return Result.ok(msg);
    }
    @GetMapping("write")
    public Result<String> write(){
        String msg = testService.writeLock();
        return Result.ok(msg);
    }

}
