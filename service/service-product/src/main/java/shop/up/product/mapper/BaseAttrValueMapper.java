package shop.up.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import shop.up.model.product.BaseAttrInfo;
import shop.up.model.product.BaseAttrValue;

/**
 * @Author: Tizzy
 * @Date: 2023/3/28 10:27
 */
@Mapper
public interface BaseAttrValueMapper extends BaseMapper<BaseAttrValue> {
}
