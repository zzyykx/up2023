package shop.up.product.service.imp;

import org.apache.commons.lang.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import shop.up.product.service.TestService;

import javax.annotation.Resource;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @Author: Tizzy
 * @Date: 2023/4/4 19:45
 */
@Service
public class TestServiceImpl implements TestService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RedissonClient redissonClient;

    @Override
    public void testLock() {

        String lockey = "lock:25";
        RLock rLock = redissonClient.getLock(lockey);
        rLock.lock();   //获得锁

        String num = stringRedisTemplate.opsForValue().get("num");
        if(StringUtils.isBlank(num)){
            return;
        }else {
            int n = Integer.parseInt(num);
            stringRedisTemplate.opsForValue().set("num",String.valueOf(++n));
        }

        rLock.unlock();   //释放锁
    }

    @Override
    public String readLock() {
        RReadWriteLock rReadWriteLock = redissonClient.getReadWriteLock("lock");
        RLock rLock = rReadWriteLock.readLock();
        rLock.lock(10, TimeUnit.SECONDS);
        String msg = stringRedisTemplate.opsForValue().get("msg");
        return "读取成功";
    }

    @Override
    public String writeLock() {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock("lock");
        RLock rLock = readWriteLock.writeLock();
        rLock.lock(10,TimeUnit.SECONDS);
        stringRedisTemplate.opsForValue().set("msg", UUID.randomUUID().toString());
        return "写入成功";
    }

//    @Override
//    public void testLock() {
//        String uuid = UUID.randomUUID().toString();
//        boolean lock = stringRedisTemplate.opsForValue().setIfAbsent("lock",uuid,1, TimeUnit.SECONDS);
//        if (lock){
//            String num = stringRedisTemplate.opsForValue().get("num");
//            if(StringUtils.isBlank(num)){
//                return;
//            }else {
//                int n = Integer.parseInt(num);
//                stringRedisTemplate.opsForValue().set("num",String.valueOf(++n));
//            }
//
////            if(uuid.equals(stringRedisTemplate.opsForValue().get("lock"))){
////                stringRedisTemplate.delete("lock");
////            }
//
//            String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
//            //  准备执行lua 脚本
//            DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
//            //  将lua脚本放入DefaultRedisScript 对象中
//            redisScript.setScriptText(script);
//            //  设置DefaultRedisScript 这个对象的泛型
//            redisScript.setResultType(Long.class);
//            //  执行删除
//            stringRedisTemplate.execute(redisScript, Arrays.asList("lock"),uuid);
//
//        }else {
//            try{
//                Thread.sleep(1000);
//                testLock();
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        }
//
//
//    }
}
