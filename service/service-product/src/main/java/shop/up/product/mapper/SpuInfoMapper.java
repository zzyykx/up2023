package shop.up.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import shop.up.model.product.SpuInfo;

/**
 * @Author: Tizzy
 * @Date: 2023/3/29 14:09
 */

public interface SpuInfoMapper extends BaseMapper<SpuInfo> {
}
