package shop.up.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import shop.up.model.product.BaseCategory3;

/**
 * @Author: Tizzy
 * @Date: 2023/3/28 10:18
 *
 * 三级分类Mapper接口
 */
@Mapper
public interface BaseCategory3Mapper extends BaseMapper<BaseCategory3> {
}
