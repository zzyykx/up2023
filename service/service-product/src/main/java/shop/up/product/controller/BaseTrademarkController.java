package shop.up.product.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import shop.up.common.result.Result;
import shop.up.model.product.BaseTrademark;
import shop.up.product.service.BaseTrademarkService;

import java.util.List;

/**
 * @Author: Tizzy
 * @Date: 2023/3/29 14:42
 */
@Api(tags = "品牌接口")
@RestController
@RequestMapping("/admin/product/baseTrademark")
public class BaseTrademarkController {

    @Autowired
    private BaseTrademarkService baseTrademarkService;


    /**
     * 加载品牌信息
     * @return
     */
    @GetMapping("getTrademarkList")
    public Result<List<BaseTrademark>> getTrademarkList() {
        List<BaseTrademark> trademarkList = baseTrademarkService.list(null);
        return Result.ok(trademarkList);
    }

    /**
     * 获取品牌信息（分页）
     * @param page
     * @param limit
     * @return
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("{page}/{limit}")
    public Result<IPage<BaseTrademark>> index(@PathVariable Long page,
                                              @PathVariable Long limit) {
        Page<BaseTrademark> p = new Page<>(page,limit);
        IPage<BaseTrademark> iPage = baseTrademarkService.page(p,null);

        return Result.ok(iPage);
    }

    @ApiOperation(value = "获取BaseTrademark")
    @GetMapping("get/{id}")
    public Result get(@PathVariable String id) {
        BaseTrademark baseTrademark = baseTrademarkService.getById(id);
        return Result.ok(baseTrademark);
    }

    @ApiOperation(value = "新增BaseTrademark")
    @PostMapping("save")
    public Result save(@RequestBody BaseTrademark banner) {
        baseTrademarkService.save(banner);
        return Result.ok();
    }

    @ApiOperation(value = "修改BaseTrademark")
    @PutMapping("update")
    public Result updateById(@RequestBody BaseTrademark banner) {
        baseTrademarkService.updateById(banner);
        return Result.ok();
    }

    @ApiOperation(value = "删除BaseTrademark")
    @DeleteMapping("remove/{id}")
    public Result remove(@PathVariable Long id) {
        baseTrademarkService.removeById(id);
        return Result.ok();
    }




    }
