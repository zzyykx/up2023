package shop.up.product.service;

public interface TestService {

   void testLock();

   String readLock();

   String writeLock();

}