package shop.up.product.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import shop.up.common.result.Result;
import shop.up.model.product.BaseSaleAttr;
import shop.up.model.product.SpuInfo;
import shop.up.product.service.imp.ManageServiceImp;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: Tizzy
 * @Date: 2023/3/29 14:20
 */
@Api(tags = "Spu信息接口")
@RequestMapping("admin/product")
@RestController
public class SpuManageController {

    @Resource
    private ManageServiceImp manageServiceImp;

    /**
     * 保存spu
     * @param spuInfo
     * @return
     */
    @PostMapping("saveSpuInfo")
    public Result saveSpuInfo(@RequestBody SpuInfo spuInfo){
        // 调用服务层的保存方法
        manageServiceImp.saveSpuInfo(spuInfo);
        return Result.ok();
    }

    /**
     * 获取销售属性
     * @return
     */
    // 销售属性http://api.up.shop/admin/product/baseSaleAttrList
    @ApiOperation("获取销售属性")
    @GetMapping("baseSaleAttrList")
    public Result baseSaleAttrList(){
        // 查询所有的销售属性集合
        List<BaseSaleAttr> baseSaleAttrList = manageServiceImp.getBaseSaleAttrList();

        return Result.ok(baseSaleAttrList);
    }

    /**
     *
     * @param page
     * @param limit
     * @param spuInfo
     * @return
     */
    @ApiOperation("获取商品信息")
    @GetMapping("{page}/{limit}")
    public Result<IPage<SpuInfo>> getSpuInfoPage(@PathVariable Long page,
                                                 @PathVariable Long limit,
                                                 SpuInfo spuInfo) {
        Page<SpuInfo> spuInfoPage = new Page<>(page, limit);
        IPage<SpuInfo> iPage = manageServiceImp.getSpuInfoPage(spuInfoPage, spuInfo);
        return Result.ok(iPage);
    }
}
