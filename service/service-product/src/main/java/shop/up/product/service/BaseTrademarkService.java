package shop.up.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import shop.up.model.product.BaseTrademark;
import shop.up.product.mapper.BaseTrademarkMapper;

/**
 * @Author: Tizzy
 * @Date: 2023/3/29 14:37
 */
public interface BaseTrademarkService extends IService<BaseTrademark> {
}
