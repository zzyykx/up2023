package shop.up.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import shop.up.model.product.BaseSaleAttr;

@Mapper
public interface BaseSaleAttrMapper extends BaseMapper<BaseSaleAttr> {
}