package shop.up.item.test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @Author: Tizzy
 * @Date: 2023/4/5 21:32
 */
public class CompletableFutureDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture future = CompletableFuture.supplyAsync(()->{
            System.out.println(Thread.currentThread().getName());
            int i = 2/0;
            return 200;
        }).whenComplete((obj,throwable)->{
            System.out.println("----------" + obj);
            System.out.println("----------" + throwable);
        }).exceptionally((throwable)->{
            System.out.println("==========" + throwable);
            return 404;
        });

        System.out.println(future.get());
    }
}
