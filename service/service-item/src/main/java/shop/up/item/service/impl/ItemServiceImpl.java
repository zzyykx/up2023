package shop.up.item.service.impl;

import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Service;
import shop.up.item.service.ItemService;
import shop.up.list.client.ListFeignClient;
import shop.up.model.product.BaseCategoryView;
import shop.up.model.product.SkuInfo;
import shop.up.model.product.SpuSaleAttr;
import shop.up.product.client.ProductFeignClient;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Author: Tizzy
 * @Date: 2023/4/2 10:00
 */
@Service
public class ItemServiceImpl implements ItemService {

    @Resource
    private ProductFeignClient productFeignClient;

    @Resource
    private ThreadPoolExecutor threadPoolExecutor;

    @Resource
    private ListFeignClient listFeignClient;


    /**
     * 获取sku详情信息
     * @param skuId
     * @return
     */
    @Override
    public Map<String, Object> getBySkuId(Long skuId) {
        Map<String,Object> result = new HashMap<>();

        CompletableFuture<SkuInfo> future = CompletableFuture.supplyAsync(() ->{
            SkuInfo skuInfo = productFeignClient.getSkuInfo(skuId);
            result.put("skuInfo",skuInfo);
            return skuInfo;
        },threadPoolExecutor);

        // 销售属性-销售属性值回显并锁定
        CompletableFuture<Void> spuSaleAttrCompletableFuture = future.thenAcceptAsync((skuInfo)->{
            List<SpuSaleAttr> spuSaleAttrList =  productFeignClient.getSpuSaleAttrListCheckBySku(skuInfo.getId(), skuInfo.getSpuId());
            result.put("spuSaleAttrList",spuSaleAttrList);
        },threadPoolExecutor);

        //获取商品最新价格
        CompletableFuture<Void> SkuPriceFuture = future.thenRunAsync(()->{
            BigDecimal skuPrice = productFeignClient.getSkuPrice(skuId);
            result.put("price",skuPrice);
        },threadPoolExecutor);

        //获取商品分类
        CompletableFuture<Void> CategoryViewFuture = future.thenAcceptAsync(skuInfo -> {
            BaseCategoryView categoryView = productFeignClient.getCategoryView(skuInfo.getCategory3Id());
            result.put("categoryView",categoryView);
        },threadPoolExecutor);

        //根据spuId 查询map 集合属性  （版本切换）
        CompletableFuture<Void> SkuValueIdsMapFuture = future.thenAcceptAsync(skuInfo -> {
            Map skuValueIdsMap =  productFeignClient.getSkuValueIdsMap(skuInfo.getSpuId());
            // 保存 json字符串
            String valuesSkuJson = JSON.toJSONString(skuValueIdsMap);
            // 保存valuesSkuJson
            result.put("valuesSkuJson",valuesSkuJson);
        },threadPoolExecutor);

        CompletableFuture<Void> incrHotScoreCompletableFuture = future.thenRunAsync(()->{
            listFeignClient.incrHotScore(skuId);
        },threadPoolExecutor);

        CompletableFuture.allOf(future,
                                spuSaleAttrCompletableFuture,
                                SkuPriceFuture,
                                CategoryViewFuture,
                                SkuValueIdsMapFuture,
                                incrHotScoreCompletableFuture).join();
        return result;
    }
}
