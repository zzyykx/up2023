package shop.up.item.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.up.common.result.Result;
import shop.up.item.service.ItemService;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("api/item")
public class ItemApiController {


    @Resource
    private ItemService itemService;


    /**
     * 获取sku详情信息
     * @param skuId
     * @return
     */
    @GetMapping("{skuId}")
    public Result getItem(@PathVariable Long skuId){
        Map<String,Object> result = itemService.getBySkuId(skuId);


        return Result.ok(result);
    }
}