package shop.up.item.test;

import java.util.concurrent.*;

/**
 * @Author: Tizzy
 * @Date: 2023/4/5 21:32
 */
public class CompletableFutureDemo3 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                50,
                500,
                30,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(1000)
        );
        CompletableFuture<String> future = CompletableFuture.supplyAsync(()->"hello");

        CompletableFuture<Void> future1 = future.thenAcceptAsync(str->{
            delaySec(1);
            System.out.println(str + "第一个线程");
        },threadPoolExecutor);

        CompletableFuture<Void> future2 = future.thenAcceptAsync(str->{
            delaySec(3);
            System.out.println(str + "第二个线程");
        },threadPoolExecutor);
    }

    private static void delaySec(int i) {
        try {
            Thread.sleep(i * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
