package shop.up.user.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.up.model.user.UserAddress;
import shop.up.user.service.UserAddressService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: Tizzy
 * @Date: 2023/4/13 20:11
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Resource
    private UserAddressService userAddressService;

    /**
     * 获取用户地址
     * @param userId
     * @return
     */
    @GetMapping("inner/findUserAddressListByUserId/{userId}")
    public List<UserAddress> findUserAddressListByUserId(@PathVariable("userId") String userId){
        return userAddressService.findUserAddressListByUserId(userId);
    }
}
