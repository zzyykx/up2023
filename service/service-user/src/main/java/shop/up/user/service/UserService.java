package shop.up.user.service;

import shop.up.model.user.UserInfo;

/**
 * @Author: Tizzy
 * @Date: 2023/4/12 16:50
 */
public interface UserService {
    /**
     * 登录方法
     * @param userInfo
     * @return
     */
    UserInfo login(UserInfo userInfo);
}
