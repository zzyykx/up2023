package shop.up.user.service;

import shop.up.model.user.UserAddress;

import java.util.List;

/**
 * @Author: Tizzy
 * @Date: 2023/4/13 20:07
 */
public interface UserAddressService {

    /**
     * 根据用户Id 查询用户的收货地址列表！
     * @param userId
     * @return
     */
    List<UserAddress> findUserAddressListByUserId(String userId);
}
