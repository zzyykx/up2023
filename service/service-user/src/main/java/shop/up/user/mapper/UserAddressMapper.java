package shop.up.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import shop.up.model.user.UserAddress;

/**
 * @Author: Tizzy
 * @Date: 2023/4/13 20:06
 */
public interface UserAddressMapper extends BaseMapper<UserAddress> {
}
