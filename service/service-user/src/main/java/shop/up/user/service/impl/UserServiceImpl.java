package shop.up.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import shop.up.model.user.UserInfo;
import shop.up.user.mapper.UserInfoMapper;
import shop.up.user.service.UserService;

import javax.annotation.Resource;

/**
 * @Author: Tizzy
 * @Date: 2023/4/12 16:51
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Override
    public UserInfo login(UserInfo userInfo) {
        String passwd = userInfo.getPasswd();
        String newPasswd = DigestUtils.md5DigestAsHex(passwd.getBytes());
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("login_name",userInfo.getLoginName());
        queryWrapper.eq("passwd",newPasswd);
        UserInfo info = userInfoMapper.selectOne(queryWrapper);
        return info;
    }
}
