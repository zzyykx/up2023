package shop.up.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import shop.up.model.user.UserAddress;
import shop.up.user.mapper.UserAddressMapper;
import shop.up.user.service.UserAddressService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: Tizzy
 * @Date: 2023/4/13 20:07
 */
@Service
public class UserAddressServiceImpl implements UserAddressService {

    @Resource
    private UserAddressMapper userAddressMapper;

    @Override
    public List<UserAddress> findUserAddressListByUserId(String userId) {
        QueryWrapper<UserAddress> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId);
        return userAddressMapper.selectList(queryWrapper);
    }
}
