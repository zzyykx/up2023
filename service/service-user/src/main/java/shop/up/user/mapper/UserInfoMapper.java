package shop.up.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import shop.up.model.user.UserInfo;

/**
 * @Author: Tizzy
 * @Date: 2023/4/12 16:52
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
