package shop.up.ware.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import shop.up.ware.bean.WareOrderTaskDetail;

@Repository
public interface WareOrderTaskDetailMapper extends BaseMapper<WareOrderTaskDetail> {
}
