package shop.up.ware.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import shop.up.ware.bean.WareOrderTask;

@Repository
public interface WareOrderTaskMapper extends BaseMapper<WareOrderTask> {
}
