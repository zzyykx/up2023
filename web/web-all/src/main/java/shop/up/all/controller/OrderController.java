package shop.up.all.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import shop.up.common.result.Result;
import shop.up.order.client.OrderFeignClient;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Author: Tizzy
 * @Date: 2023/4/13 20:58
 */
@Controller
public class OrderController {
    @Resource
    private OrderFeignClient orderFeignClient;


    /**
     * 确认订单
     * @param model
     * @return
     */
    @GetMapping("trade.html")
    public String trade(Model model) {
        Result<Map<String, Object>> result = orderFeignClient.trade();

        model.addAllAttributes(result.getData());
        return "order/trade";
    }
}
