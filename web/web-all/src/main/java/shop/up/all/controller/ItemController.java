package shop.up.all.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.up.common.result.Result;
import shop.up.item.client.ItemFeignClient;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Author: Tizzy
 * @Date: 2023/4/2 13:28
 */
@Controller
public class ItemController {

    @Resource
    private ItemFeignClient itemFeignClient;

    @RequestMapping("{skuId}.html")
    public String getItem(@PathVariable Long skuId, Model model) {
        // 通过skuId 查询skuInfo
        Result<Map> result = itemFeignClient.getItem(skuId);
        model.addAllAttributes(result.getData());
        return "item/index";
    }
}
