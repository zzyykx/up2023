package shop.up.common.cache;

import java.lang.annotation.*;

@Target(ElementType.METHOD) //  注解使用范围
@Retention(RetentionPolicy.RUNTIME) //  注解的生命周期
public @interface UpSystemCache {

    //  定义一个组成缓存中 key 的前缀！
    String prefix() default "cache";
}
