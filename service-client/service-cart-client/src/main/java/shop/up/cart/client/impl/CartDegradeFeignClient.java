package shop.up.cart.client.impl;

import org.springframework.stereotype.Component;
import shop.up.cart.client.CartFeignClient;
import shop.up.common.result.Result;
import shop.up.model.cart.CartInfo;

import java.util.List;

/**
 * @Author: Tizzy
 * @Date: 2023/4/12 23:33
 */
@Component
public class CartDegradeFeignClient implements CartFeignClient {
    @Override
    public Result addToCart(Long skuId, Integer skuNum) {
        return null;
    }

    @Override
    public List<CartInfo> getCartCheckedList(String userId) {
        return null;
    }

    @Override
    public Result loadCartCache(String userId) {
        return null;
    }
}
