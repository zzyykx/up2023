package shop.up.user.client.impl;

import org.springframework.stereotype.Component;
import shop.up.model.user.UserAddress;
import shop.up.user.client.UserFeignClient;

import java.util.List;

/**
 * @Author: Tizzy
 * @Date: 2023/4/13 20:15
 */
@Component
public class UserDegradeFeignClient implements UserFeignClient {
    @Override
    public List<UserAddress> findUserAddressListByUserId(String userId) {
        return null;
    }
}
