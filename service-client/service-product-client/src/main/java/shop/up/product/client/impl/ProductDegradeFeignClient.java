package shop.up.product.client.impl;

import org.springframework.stereotype.Component;
import shop.up.common.result.Result;
import shop.up.model.product.*;
import shop.up.product.client.ProductFeignClient;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Author: Tizzy
 * @Date: 2023/4/2 12:43
 */
@Component
public class ProductDegradeFeignClient implements ProductFeignClient {
    @Override
    public BaseTrademark getTrademark(Long tmId) {
        return null;
    }

    @Override
    public List<BaseAttrInfo> getAttrList(Long skuId) {
        return null;
    }

    @Override
    public Result getBaseCategoryList() {
        return null;
    }

    @Override
    public SkuInfo getSkuInfo(Long skuId) {
        return null;
    }

    @Override
    public BaseCategoryView getCategoryView(Long category3Id) {
        return null;
    }

    @Override
    public BigDecimal getSkuPrice(Long skuId) {
        return null;
    }

    @Override
    public List<SpuSaleAttr> getSpuSaleAttrListCheckBySku(Long skuId, Long spuId) {
        return null;
    }

    @Override
    public Map getSkuValueIdsMap(Long spuId) {
        return null;
    }
}
