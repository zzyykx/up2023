package shop.up.order.client.impl;

import org.springframework.stereotype.Component;
import shop.up.common.result.Result;
import shop.up.order.client.OrderFeignClient;

import java.util.Map;

/**
 * @Author: Tizzy
 * @Date: 2023/4/13 20:49
 */
@Component
public class OrderDegradeFeignClient implements OrderFeignClient {
    @Override
    public Result<Map<String, Object>> trade() {
        return Result.fail();
    }
}
