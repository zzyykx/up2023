package shop.up.item.client.impl;

import org.springframework.stereotype.Component;
import shop.up.common.result.Result;
import shop.up.item.client.ItemFeignClient;

/**
 * @Author: Tizzy
 * @Date: 2023/4/2 13:25
 */
@Component
public class ItemDegradeFeignClient implements ItemFeignClient {
    @Override
    public Result getItem(Long skuId) {
        return Result.fail();
    }
}
