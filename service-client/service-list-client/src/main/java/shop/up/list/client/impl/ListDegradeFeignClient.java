package shop.up.list.client.impl;

import org.springframework.stereotype.Component;
import shop.up.common.result.Result;
import shop.up.list.client.ListFeignClient;
import shop.up.model.list.SearchParam;

/**
 * @Author: Tizzy
 * @Date: 2023/4/10 16:22
 */
@Component
public class ListDegradeFeignClient implements ListFeignClient {
    @Override
    public Result list(SearchParam searchParam) {
        return null;
    }

    @Override
    public Result incrHotScore(Long skuId) {
        return null;
    }

    @Override
    public Result upperGoods(Long skuId) {
        return null;
    }

    @Override
    public Result lowerGoods(Long skuId) {
        return null;
    }
}
